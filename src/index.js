const btn1 = document.getElementById("btn1");
const btn2 = document.getElementById("btn2");
const btn3 = document.getElementById("btn3");
let texto = document.getElementById('texto');
let clave = document.getElementById('clave');
let resultado = document.getElementById('resultado');

btn1.addEventListener('click', (event) => {
  event.preventDefault();
  if (clave.value) {
    resultado.value = window.cipher.encode(texto.value, clave.value);
  } else {
    alert('ingrese una clave')
  }
});
btn2.addEventListener('click', (event) => {
  event.preventDefault();
  if (clave.value) {
    resultado.value = window.cipher.decode(texto.value, clave.value);
  }else {
    alert('ingrese una clave')
  }
});

btn3.addEventListener('click', (event) => {
  event.preventDefault();
  texto.value = '';
  clave.value = '';
  resultado.value = '';
})

//Menú amburguesa
let open = document.getElementById('open');
let menu = document.getElementById('menu');
let cerrado = true;
open.addEventListener('click', () => {
  if (cerrado) {
    menu.style.width = '30vw';
    cerrado = false;
  }
  else {
    menu.style.width = '0%';
    menu.style.overflow = 'hidden';
    cerrado = true;

  }
});

window.addEventListener('resize', () => {
  if (screen.width > 850) {
    menu.style.removeProperty('overflow');
    menu.style.removeProperty('width');
  }
});
